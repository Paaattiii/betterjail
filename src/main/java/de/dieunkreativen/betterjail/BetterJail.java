package de.dieunkreativen.betterjail;

import java.util.logging.Logger;

import de.dieunkreativen.betterjail.handler.ConfigHandler;
import de.dieunkreativen.betterjail.handler.PlayerEventHandler;
import net.milkbowl.vault.economy.Economy;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

public class BetterJail extends JavaPlugin {
	static BetterJail plugin;
	public static Economy econ = null;
	Logger log;
	PluginDescriptionFile pdfFile = this.getDescription();
	public final ConfigHandler confighandler = new ConfigHandler(this);
	private final PlayerEventHandler playerhandler = new PlayerEventHandler(this);
	
    @Override
    public void onLoad() {
    	log = getLogger();
    }
	
	@Override
	public void onEnable() {
		plugin = this;
		setupEconomy();
		PluginManager pm = this.getServer().getPluginManager();
        pm.registerEvents(playerhandler, this);
        Scheduler scheduler = new Scheduler(this);
        Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(this, scheduler, 1200L, 1200L);
        
		log.info(pdfFile.getName() + " version " + pdfFile.getVersion() + " is enabled!");
		confighandler.loadConfig();
	}
	
	@Override
	public void onDisable() {
		log.info(pdfFile.getName() + " version " + pdfFile.getVersion() + " is disabled!");
	}
	
	
    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }
	
	public static BetterJail getSelf() {
		return plugin;
	}
	
	@Override
	public boolean onCommand(final CommandSender sender, Command cmd, String label, final String[] args) {	
		if (cmd.getName().equalsIgnoreCase("jail") && sender.hasPermission("jail.use")) {
			if (args.length >= 3) {
				if(isInteger(args[1])) {
					jailPlayer(args[0], Integer.parseInt(args[1]), parseReason(args), sender);
				}
				else {
					sender.sendMessage(ChatColor.GOLD + "[Jail] " + ChatColor.RED + args[1] + " ist keine g�ltige Zeitangabe!");
				}
			}
			else {
				sender.sendMessage(ChatColor.GOLD + "[Jail] " + ChatColor.RED + "/jail <Spieler> <Zeit> <Grund>");
			}
			return true;
		}
		
		if (cmd.getName().equalsIgnoreCase("unjail") && sender.hasPermission("jail.use")) {
			if (args.length == 1) {
				unjailPlayer(args[0], sender);
			}
			else {
				sender.sendMessage(ChatColor.GOLD + "[Jail] " + ChatColor.RED + "/unjail <Spieler>");
			}
			return true;
		}
		
		if (cmd.getName().equalsIgnoreCase("jailcheck") && sender.hasPermission("jail.use")) {
			if (args.length == 1) {
				jailCheck(args[0], sender);
			}
			else {
				sender.sendMessage(ChatColor.GOLD + "[Jail] " + ChatColor.RED + "/jailcheck <Spieler>");
			}
			return true;
		}
		
		if (cmd.getName().equalsIgnoreCase("jailreload") && sender.hasPermission("jail.admin")) {
			confighandler.loadConfig();
			sender.sendMessage(ChatColor.GOLD + "[Jail] " + ChatColor.GREEN + "Konfiguration neu geladen!");
			return true;
		}
		
		if (cmd.getName().equalsIgnoreCase("jailcreate") && sender.hasPermission("jail.admin")) {
			if (sender instanceof Player) {
				Player p = (Player)sender;
				confighandler.addCell(p.getLocation());
				p.sendMessage(ChatColor.GOLD + "[Jail] " + ChatColor.GREEN + "Zelle wurde erstellt!");
			}
			return true;
		}
		if (cmd.getName().equalsIgnoreCase("handcuff") && sender.hasPermission("jail.use")) {
			if (args.length == 1) {
				if(!PlayerEventHandler.handcuffPlayer.contains(args[0])) {
					if(isPlayerOnline(args[0])) {
						PlayerEventHandler.handcuffPlayer.add(args[0]);
						sender.sendMessage(ChatColor.GOLD + "[Jail] " + ChatColor.GREEN + args[0] + " wurden Handschellen angelegt!");
					}
					else {
						sender.sendMessage(ChatColor.GOLD + "[Jail] " + ChatColor.RED + args[0] + " ist nicht online!");
					}
				}
				else {
					sender.sendMessage(ChatColor.GOLD + "[Jail] " + ChatColor.RED + args[0] + " wurden bereits Handschellen angelegt!");
				}
			}
			else {
				sender.sendMessage(ChatColor.GOLD + "[Jail] " + ChatColor.RED + "/handcuff <Spieler>");
			}
			return true;
		}
		if (cmd.getName().equalsIgnoreCase("unhandcuff") && sender.hasPermission("jail.use")) {
			if (args.length == 1) {
				if(PlayerEventHandler.handcuffPlayer.contains(args[0])) {
					PlayerEventHandler.handcuffPlayer.remove(args[0]);
					sender.sendMessage(ChatColor.GOLD + "[Jail] " + ChatColor.GREEN + args[0] + " wurde von den Handschellen befreit!");
				}
				else {
					sender.sendMessage(ChatColor.GOLD + "[Jail] " + ChatColor.RED + args[0] + " tr�gt keine Handschellen!");
				}
			}
			else {
				sender.sendMessage(ChatColor.GOLD + "[Jail] " + ChatColor.RED + "/unhandcuff <Spieler>");
			}
			return true;
		}
		sender.sendMessage(ChatColor.GOLD + "[Jail] " + ChatColor.DARK_RED + " Dazu hast du keine Berechtigung!");
		return false;
	}
	
	@SuppressWarnings("deprecation")
	public void jailPlayer(final String p, final int time, final String reason, final CommandSender sender) {
            	String uuid = null;
            	
            	if (isPlayerOnline(p)) {
            		uuid = Bukkit.getPlayer(p).getUniqueId().toString();
            	}
            	else {
            		if (Bukkit.getOfflinePlayer(p).hasPlayedBefore()) {
            			uuid = Bukkit.getOfflinePlayer(p).getUniqueId().toString();
            		}
            		else {
            			sender.sendMessage(ChatColor.GOLD + "[Jail] " + ChatColor.RED + p + " war noch nie auf diesen Server!");
            		}
            	}
            	
            	if (uuid != null) {
            		if (!confighandler.isJailed(uuid)) {
            			if(isPlayerOnline(p)) {
            				Player player = Bukkit.getPlayer(p);
            				PlayerEventHandler.addToPrisonerList(uuid);
            				confighandler.addPrisoner(uuid, p, time, reason, player.getLocation(), getFine(time, player));
            				String cell = plugin.confighandler.getPrisonerCell(uuid);
            				Location loc = plugin.confighandler.getCellLocation(cell);
            				player.teleport(loc);
            				sender.sendMessage(ChatColor.GOLD + "[Jail] " + ChatColor.GREEN + p + " sitzt nun im Gef�ngnis!");
            				player.sendMessage(ChatColor.GOLD + "[Jail] " + ChatColor.WHITE + "Du bist f�r " + time + " Minuten im Gef�ngnis!");
            				player.sendMessage(ChatColor.GOLD + "[Jail] " + ChatColor.RED + "Grund: " + reason);
            			}
            			else {
            				Location loc = null;
            				confighandler.addPrisoner(uuid, p, time, reason, loc, getFine(time, Bukkit.getOfflinePlayer(uuid)));
            				sender.sendMessage(ChatColor.GOLD + "[Jail] " + ChatColor.GREEN + p + " wird bei seinen n�chsten Login in das Gef�ngnis gesteckt!");
            			}
            			PlayerEventHandler.handcuffPlayer.remove(p);
            		}
            		else {
            			sender.sendMessage(ChatColor.GOLD + "[Jail] " + ChatColor.RED + p + " ist bereits im Gef�ngnis!");
            		}
                }
	}
	
	@SuppressWarnings("deprecation")
	public void unjailPlayer(final String p, final CommandSender sender) {
            	String uuid = null;
            	
            	if (isPlayerOnline(p)) {
            		uuid = Bukkit.getPlayer(p).getUniqueId().toString();
            	}
            	else {
            		OfflinePlayer op = Bukkit.getOfflinePlayer(p);
            		if (op.hasPlayedBefore()) {
            			uuid = op.getUniqueId().toString();
            			confighandler.setPlayerRelease(uuid);
            			sender.sendMessage(ChatColor.GOLD + "[Jail] " + ChatColor.GREEN + p + " wird bei seinen n�chsten Login entlassen!");
            		}
            		else {
            			sender.sendMessage(ChatColor.GOLD + "[Jail] " + ChatColor.RED + p + " war noch nie auf diesen Server!");
            		}
            	}
            	
            	if (uuid != null) {
            		if (confighandler.isJailed(uuid)) {
            			if(isPlayerOnline(p)) {
            				Location lastLocation = plugin.confighandler.getLastPlayerLocation(uuid);
            				Bukkit.getPlayer(p).teleport(lastLocation);
            				confighandler.removePrisoner(uuid.toString());
            				sender.sendMessage(ChatColor.GOLD + "[Jail] " + ChatColor.GREEN + p + " ist entlassen wurden!");
            				PlayerEventHandler.prisoner.remove(uuid);
            			}
            		}
            		else {
            			sender.sendMessage(ChatColor.GOLD + "[Jail] " + ChatColor.RED + p + " ist nicht im Gef�ngnis!");
            		}
            	}
	}
	
	@SuppressWarnings("deprecation")
	public void jailCheck(final String p, final CommandSender sender) {
		String uuid = null;
    	
    	if (isPlayerOnline(p)) {
    		uuid = Bukkit.getPlayer(p).getUniqueId().toString();
    	}
    	else {
    		OfflinePlayer op = Bukkit.getOfflinePlayer(p);
    		if (op.hasPlayedBefore()) {
    			uuid = op.getUniqueId().toString();
    		}
    		else {
    			sender.sendMessage(ChatColor.GOLD + "[Jail] " + ChatColor.RED + p + " war noch nie auf diesen Server!");
    		}
    	}
    	
    	if (uuid != null) {
    		if (confighandler.isJailed(uuid)) {
    			int time = confighandler.getReleaseTime(uuid);
    			String reason = confighandler.getReason(uuid);
    			String cellID = confighandler.getPrisonerCell(uuid);
    			double fine = confighandler.getFine(uuid);
    			
    			sender.sendMessage(ChatColor.GREEN + "---------- " + ChatColor.GOLD + "[Jail]" + ChatColor.GREEN +  " ----------");
    			sender.sendMessage(ChatColor.RED +"UUID: " + ChatColor.WHITE + uuid);
    			sender.sendMessage(ChatColor.RED +"Letzter Name: " + ChatColor.WHITE + p);
    			sender.sendMessage(ChatColor.RED +"Gef�ngniszelle: " + ChatColor.WHITE + cellID);
    			sender.sendMessage(ChatColor.RED +"Grund: " + ChatColor.WHITE + reason);
    			sender.sendMessage(ChatColor.RED +"Restliche Haftzeit: " + ChatColor.WHITE + time + " Minuten");
    			sender.sendMessage(ChatColor.RED + "Geldstrafe: " + ChatColor.GOLD + fine + " Euro");
    		}
    		else {
    			sender.sendMessage(ChatColor.GOLD + "[Jail] " + ChatColor.RED + p + " befindet sich nicht im Gef�ngnis!");
    		}
    	}
	}
	
	
	@SuppressWarnings("deprecation")
	public boolean isPlayerOnline(String name){
		for (Player onlinePlayer : Bukkit.getOnlinePlayers()){
			if (onlinePlayer.getName().equalsIgnoreCase(name))
				return true;
		}
		return false;
	}
	
	public String parseReason (String [] args) {
		StringBuilder buffer = new StringBuilder();

		// change the starting i value to pick what argument to start from
		// 1 is the 2nd argument.
		for(int i = 2; i < args.length; i++)
		{
		    buffer.append(' ').append(args[i]);
		}
		
		return buffer.toString();
	}
	
    public boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }
    
    public double getFine(int time, OfflinePlayer p) {
    	double fine = 0;
    	if (time >= 60) {
    		fine = econ.getBalance(p) * 0.50;
    	}
    	else if (time >= 30) {
    		fine = econ.getBalance(p) * 0.25;
    	}
    	else if (time >= 10) {
    		fine = econ.getBalance(p) * 0.10;
    	}
    	econ.withdrawPlayer(p, fine);
    	return fine;
    }
}

