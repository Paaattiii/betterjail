package de.dieunkreativen.betterjail.handler;

import java.io.File;
import java.io.IOException;

import de.dieunkreativen.betterjail.BetterJail;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class ConfigHandler {
	BetterJail plugin;
	
	public ConfigHandler(BetterJail instance) {
		plugin = instance;
	}
	
	//Prisoners.yml
	FileConfiguration configp;
	File filep;
	
	//Jail.yml
	FileConfiguration configc;
	File filec;
	
	
	public void loadConfig() {
		filep = new File(BetterJail.getSelf().getDataFolder()+File.separator+"Prisoners.yml");
		configp = YamlConfiguration.loadConfiguration(filep);
		
		filec = new File(BetterJail.getSelf().getDataFolder()+File.separator+"Jail.yml");
		configc = YamlConfiguration.loadConfiguration(filec);
	}
	
	public void addPrisoner(String uuid, String playername, int time, String reason, Location lastLocation, double fine) {
		configp.set(uuid +".name", playername);
		configp.set(uuid +".time", time);
		configp.set(uuid +".reason", reason);
		configp.set(uuid +".jailed", true);
		configp.set(uuid +".cellID", getRandomCell());
		configp.set(uuid +".fine", fine);
		addPrisonerLocation(uuid, lastLocation);
		try {
			configp.save(filep);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void addPrisonerLocation(String uuid, Location lastLocation) {
		if (lastLocation != null) {
			configp.set(uuid +".lastLocation.x", lastLocation.getX());
			configp.set(uuid +".lastLocation.y", lastLocation.getY());
			configp.set(uuid +".lastLocation.z", lastLocation.getZ());
			configp.set(uuid +".lastLocation.world", lastLocation.getWorld().getName());
			try {
				configp.save(filep);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	public void removePrisoner(String uuid) {
		if(isJailed(uuid)) {
			configp.set(uuid, null);
			try {
				configp.save(filep);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	public void addCell(Location loc) {
		final int id = getNextCell();
		configc.set(id + ".x", loc.getX());
		configc.set(id + ".y", loc.getY());
		configc.set(id + ".z", loc.getZ());
		configc.set(id + ".world", loc.getWorld().getName());
		
		try {
			configc.save(filec);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public int getNextCell() {
	    for(int i=0; ;i++)
	    {
	    	if (configc.get(String.valueOf(i)) == null) {
	    		return i;
	    	}
	    } 
	}
	
	public int getRandomCell() {
		int randomCell = (int) Math.abs(Math.random()*getNextCell());
		return randomCell;
	}
	
	public Location getCellLocation(String id) {
		World world = Bukkit.getServer().getWorld(configc.getString(id + ".world"));
		double x = configc.getDouble(id + ".x");
		double y = configc.getDouble(id + ".y");
		double z = configc.getDouble(id + ".z");
		Location loc = new Location(world, x, y, z);
		return loc;
	}
	
	public Location getLastPlayerLocation(String uuid) {
		if (configp.get(uuid +".lastLocation") != null) {
			World world = Bukkit.getServer().getWorld(configp.getString(uuid +".lastLocation.world"));
			double x = configp.getDouble(uuid +".lastLocation.x");
			double y = configp.getDouble(uuid +".lastLocation.y");
			double z = configp.getDouble(uuid +".lastLocation.z");
			Location loc = new Location(world, x, y, z);
			return loc;
		}
		return null;
	}
	
	public String getPrisonerCell(String uuid) {
		String id = configp.getString(uuid + ".cellID");
		return id;
	}
	
	public String getReason(String uuid) {
		String reason = configp.getString(uuid + ".reason");
		return reason;
	}
	
	public void setPlayerRelease(String uuid) {
		configp.set(uuid +".jailed", false);
		try {
			configp.save(filep);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public boolean isReleased(String uuid) {
		if (configp.getBoolean(uuid +".jailed") == true) {
			return false;
		}
		return true;
	}
	
	public int getReleaseTime(String uuid) {
		int time = configp.getInt(uuid +".time");
		return time;
	}
	
	public void reduceTime(String uuid) {
		int time = getReleaseTime(uuid) - 1;
		configp.set(uuid +".time", time);
		try {
			configp.save(filep);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public boolean isJailed(String uuid) {
		if(configp.get(uuid) == null) {
			return false;
		}
		return true;
	}
	
	public double getFine(String uuid) {
		if(configp.get(uuid +".fine") != null) {
			double time = configp.getDouble(uuid +".fine");
			return time;
		}
		return 0;		
	}
	
	
	
	

}

