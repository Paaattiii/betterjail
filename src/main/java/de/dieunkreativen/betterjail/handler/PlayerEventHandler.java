package de.dieunkreativen.betterjail.handler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import de.dieunkreativen.betterjail.BetterJail;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.BlockState;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

public class PlayerEventHandler implements Listener {
	BetterJail plugin;
	public static int task1;
	public static List<String> prisoner = new ArrayList<String>();
	public static List<String> handcuffPlayer = new ArrayList<String>();
	public static HashMap<String, Long> lastMove = new HashMap<String, Long>();
	HashMap<Player, String> lastMessage = new HashMap<Player, String>();
	
	public PlayerEventHandler(BetterJail instance) {
		plugin = instance;
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
    public void onInteract(PlayerInteractEvent event) {
		Player p = event.getPlayer();
		if(prisoner.contains(p.getUniqueId().toString())) {
			if(event.getPlayer().getItemInHand().getType() == Material.ENDER_PEARL && (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK)) {
				p.sendMessage(ChatColor.GOLD + "[Jail] " + ChatColor.RED + "Du darfst das nicht benutzen!");
				event.setCancelled(true);
			}
		}
	}
	
	@EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
		Player p = event.getPlayer();
		if(prisoner.contains(p.getUniqueId().toString()) || handcuffPlayer.contains(p.getName())) {
			p.sendMessage(ChatColor.GOLD + "[Jail] " + ChatColor.RED + "Du darfst hier keine Bl�cke abbauen!");
			event.setCancelled(true);
		}
	}
	
	@EventHandler
    public void onPlayerRespawn(PlayerRespawnEvent event) {
		final Player p = event.getPlayer();
		if(prisoner.contains(p.getUniqueId().toString())) {
			task1 = Bukkit.getScheduler().scheduleSyncDelayedTask(BetterJail.getSelf(), new Runnable() {
				public void run() {
					p.sendMessage(ChatColor.GOLD + "[Jail] " + ChatColor.RED + "Du wurdest zur�ck in deine Zelle teleportiert!!");
					String cell = plugin.confighandler.getPrisonerCell(p.getUniqueId().toString());
					Location loc = plugin.confighandler.getCellLocation(cell);
					p.teleport(loc);
				}
	        }, 10L);
		}
	}
	
	
	@EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
		Player p = event.getPlayer();
		if(prisoner.contains(p.getUniqueId().toString()) || handcuffPlayer.contains(p.getName())) {
			p.sendMessage(ChatColor.GOLD + "[Jail] " + ChatColor.RED + "Du darfst hier keine Bl�cke platzieren!");
			event.setCancelled(true);
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerChat(AsyncPlayerChatEvent event) {
		Player p = event.getPlayer();
		String m = event.getMessage();
		List<String> whitelist = Arrays.asList("hallo", "huhu", "hi", "willkommen", "xd");
		
		if(!p.hasPermission("jail.chat.exempt")) {
			if (lastMessage.containsKey(p) && !whitelist.contains(m.toLowerCase())) {
				if (LevenshteinDistance(m, lastMessage.get(p)) < m.length() / 3) {
					p.sendMessage(ChatColor.GOLD + "[Jail] " + ChatColor.RED + "Es reicht, wenn du es EINMAL schreibst!");
					event.setCancelled(true);
				}
				else {
					if (m.length() > 3) {
						double x = m.length()*0.4;
						if(getCapsSize(m) > x) {
							p.sendMessage(ChatColor.GOLD + "[Jail] " + ChatColor.RED + "Bitte nicht so viele Gro�buchstaben (Caps) verwenden!");
				    		event.setMessage(m.toLowerCase());
						}
					}
				}
			}
			lastMessage.put(p, m);
		}
		
		if(prisoner.contains(p.getUniqueId().toString())) {
			p.sendMessage(ChatColor.GOLD + "[Jail] " + ChatColor.RED + "Du darfst nicht mit anderen sprechen!");
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onCommand(PlayerCommandPreprocessEvent event) {
		Player p = event.getPlayer();
		lastMessage.put(p, "");
		if(prisoner.contains(p.getUniqueId().toString())) {
			p.sendMessage(ChatColor.GOLD + "[Jail] " + ChatColor.RED + "Du darfst keine Befehle benutzen!");
			event.setCancelled(true);
		}
	}
	
	@EventHandler
    public void onPlayerMove(PlayerMoveEvent event) {
		Player p = event.getPlayer();
		if(prisoner.contains(p.getUniqueId().toString())) {
			if(event.getTo().getBlockX() != event.getFrom().getBlockX() || event.getTo().getBlockZ() != event.getFrom().getBlockZ()) {
				 long date = new Date().getTime();
				 lastMove.put(p.getName(), date);
		        } 
		}
		if(handcuffPlayer.contains(p.getName())) {
			if(event.getTo().getBlockX() != event.getFrom().getBlockX() || event.getTo().getBlockZ() != event.getFrom().getBlockZ() || event.getTo().getBlockY() != event.getFrom().getBlockY()) {
				p.teleport(event.getFrom());
				p.sendMessage(ChatColor.GOLD + "[Jail] " + ChatColor.RED + "Dir wurden Handschellen angelegt. Du kannst dich nicht mehr bewegen!");
			} 
		}
	}
	
	@EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
		Player p = event.getPlayer();
		String uuid = p.getUniqueId().toString();
		lastMessage.put(p, "");
		
		if(plugin.confighandler.isJailed(uuid)) {
			if(plugin.confighandler.getLastPlayerLocation(uuid) == null) {
				plugin.confighandler.addPrisonerLocation(uuid, p.getLocation());
			}
			if(plugin.confighandler.isReleased(uuid)) {
				Location lastLocation = plugin.confighandler.getLastPlayerLocation(uuid);
				p.teleport(lastLocation);
				plugin.confighandler.removePrisoner(uuid);
			}
			else {
				addToPrisonerList(uuid);
				String cell = plugin.confighandler.getPrisonerCell(uuid);
				Location loc = plugin.confighandler.getCellLocation(cell);
				p.teleport(loc);
				p.sendMessage(ChatColor.GOLD + "[Jail] " + ChatColor.WHITE + "Du bist f�r " + plugin.confighandler.getReleaseTime(uuid) + " Minuten im Gef�ngnis!");
				p.sendMessage(ChatColor.GOLD + "[Jail] " + ChatColor.RED + "Grund: " + plugin.confighandler.getReason(uuid));
				if(plugin.confighandler.getFine(uuid) != 0) {
					p.sendMessage(ChatColor.GOLD + "[Jail] " + ChatColor.RED + "Geldstrafe: " + ChatColor.GOLD + plugin.confighandler.getFine(uuid) + " Euro");
				}
			}
		}
    }
	
	@EventHandler
    public void onPlayerLeave(PlayerQuitEvent event) {
		Player p = event.getPlayer();
		lastMessage.remove(p);
		prisoner.remove(p.getUniqueId().toString());
	}
	
	@EventHandler
    public void onPlayerKick(PlayerKickEvent event) {
		Player p = event.getPlayer();
		lastMessage.remove(p);
		prisoner.remove(p.getUniqueId().toString());
	}
	
	
	public static void addToPrisonerList(String uuid) {
		if (!prisoner.contains(uuid)) {
			prisoner.add(uuid);
		}
	}
	
	private int getCapsSize(String message){
		int counter = 0;
		for(int i = 0; i<message.length(); i++) {
			if(Character.isUpperCase(message.charAt(i)) && Character.isLetter(message.charAt(i))) {
				counter++;
			}
		}
		return counter;
	}
	
	public int LevenshteinDistance (String s0, String s1) {                          
	    int len0 = s0.length() + 1;                                                     
	    int len1 = s1.length() + 1;                                                     
	 
	    // the array of distances                                                       
	    int[] cost = new int[len0];                                                     
	    int[] newcost = new int[len0];                                                  
	 
	    // initial cost of skipping prefix in String s0                                 
	    for (int i = 0; i < len0; i++) cost[i] = i;                                     
	 
	    // dynamicaly computing the array of distances                                  
	 
	    // transformation cost for each letter in s1                                    
	    for (int j = 1; j < len1; j++) {                                                
	        // initial cost of skipping prefix in String s1                             
	        newcost[0] = j;                                                             
	 
	        // transformation cost for each letter in s0                                
	        for(int i = 1; i < len0; i++) {                                             
	            // matching current letters in both strings                             
	            int match = (s0.charAt(i - 1) == s1.charAt(j - 1)) ? 0 : 1;             
	 
	            // computing cost for each transformation                               
	            int cost_replace = cost[i - 1] + match;                                 
	            int cost_insert  = cost[i] + 1;                                         
	            int cost_delete  = newcost[i - 1] + 1;                                  
	 
	            // keep minimum cost                                                    
	            newcost[i] = Math.min(Math.min(cost_insert, cost_delete), cost_replace);
	        }                                                                           
	 
	        // swap cost/newcost arrays                                                 
	        int[] swap = cost; cost = newcost; newcost = swap;                          
	    }                                                                               
	 
	    // the distance is the cost for transforming all letters in both strings        
	    return cost[len0 - 1];                                                          
	}

}

